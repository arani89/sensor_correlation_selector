import pandas as pd
import numpy as np
import matplotlib as mlt
import matplotlib.pyplot as plt
import random
import math
from joblib import Parallel, delayed
import multiprocessing as mp

#Read the energy at each locations from our energy data file, and stores it in a dataframe
def read_csv(filename):
    df = pd.read_csv(filename, header=None)
                     # names=['xpos',
                     #        'ypos',
                     #        'snr',
                     #        'pd',
                     #        'type'
                     #        'sum',
                     #        'numSamples',
                     #        'nFFT'])
    #print (df)
    edf = df[df.iloc[:, 4] == 'energy']
    return edf

#Gets the transmitter location from the dataframe
#Since this is not explicitly given, we assume that
#the point with highest signal energy is the transmitter
def get_transmitter_loc_data(edf):
    x_pos = edf.loc[edf[3].idxmax()][0]
    y_pos = edf.loc[edf[3].idxmax()][1]
    tx = (x_pos, y_pos)
    #print (tx)
    return tx

#Compute euclidean distance
def euclidean(x1, y1, x2, y2):
    return np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

#Generate a transmitter based on given prior probabilities
def get_transmitter_loc_prior():
    toss = random.uniform(0, 1)
    intruder_position = np.searchsorted(cum, toss)

    intruder_x = int(intruder_position % width)
    intruder_y = int(intruder_position / width)
    return (intruder_x, intruder_y)

#Create a new dataframe storing the distance of sensors from the transmitters
def create_pd_model(edf, tx):
    model_df = edf
    model_df[10] = model_df.apply(lambda row: euclidean(row[0], row[1], tx[0], tx[1]), axis=1)
    pd_model = model_df.iloc[:, [0, 1, 6, 7, 10, 2, 3]]
    return pd_model
    #model_df = edf.icol()

#Gets probability of detection of a sensor based on distance and given software parameters
#Software Parameters = (number of Samples, FFT bin size)
def get_pd_val(numSamples, numFFT, distance):
    scaling_factor = 0.5
    pd_val = 1
    if (distance < 50 * scaling_factor):
        pd_val = predictor[numSamples, numFFT](distance / scaling_factor)
    else:
        pd_val = pfa - 0.001

    if (pd_val < pfa):
        pd_val = pfa - 0.001
    return pd_val

#Remove outliers in the model by using a polynomial regression based on distance
def sanitize_model(pd_model):
    numSamples = 32
    fit_fn = {}
    while (numSamples <= 4096):
        numFFT = 32
        while (numFFT <= numSamples):
            pd_model_temp = pd_model[(pd_model[6] == numSamples) & (pd_model[7] == numFFT)]
            fit = np.polyfit(pd_model_temp[10], pd_model_temp[3], 2)
            fit_fn[(numSamples, numFFT)] = np.poly1d(fit)
            numFFT *= 2
        numSamples *= 2

    pd_model[11] = pd_model.apply(lambda row: fit_fn[(row[6], row[7])](row[10]), axis=1)
    pd_model = pd_model.sort_values(11)
    #print ('pd_model before changing = ', pd_model)
    return (pd_model, fit_fn)

#Assign column names to the sensor dataframe
def assign_column_names(simulated_data, nss):
    column_names = []
    column_names.append('t_loc_x')
    column_names.append('t_loc_y')
    for i in range(0, nss):
        column_names.append('s' + str(i))

    simulated_data = pd.DataFrame(simulated_data, columns=column_names)
    return simulated_data

#Wrapper class to run simulations
def simulate(pd_model):
    nss = pd_model.shape[0]
    print('shape = ', pd_model.shape)
    t_loc = [get_transmitter_loc_prior() for i in range(0, int(num_simulations / 2))]
    t_loc.extend([(float("inf"), float("inf")) for i in range(int(num_simulations / 2), num_simulations)])


    simulated_data = Parallel(n_jobs=mp.cpu_count())((delayed(simulate_one_instance)
                                     (t_loc[i], pd_model)
                                     for i in range(int(num_simulations))))
    simulated_data = np.array(simulated_data)
    #print (simulated_data)
    simulated_data = assign_column_names(simulated_data, nss)
    return simulated_data

import sys

#Simulate one transmitter case
def simulate_one_instance(t_loc, pd_model):
    simulated_model = [0] * (2 + pd_model.shape[0])
    simulated_model[0] = t_loc[0]
    simulated_model[1] = t_loc[1]
    #print(i, pd_model.shape)
    #for index, row in pd_model.iterrows():
    for index in range(pd_model.shape[0]):
        distance = euclidean(t_loc[0], t_loc[1], pd_model.loc[index, 'xPos'], pd_model.loc[index, 'yPos'])
        pd_value = get_pd_val(pd_model.loc[index, 'numSamples'], pd_model.loc[index, 'nFFT'], distance)
     #print('distance = ', distance, 'pd = ', pd_value)
        if (pd_value < pfa):
           pd_value = pfa - 0.001
        toss_value = random.uniform(0, 1)
        sensor_value = 0
        if (toss_value < pd_value):
         sensor_value = 1
        #print(index)
        simulated_model[2 + index] = sensor_value
    return simulated_model

#Generate sensor data for case 1
#Case 1 -> All sensors have identical parameters
def get_case_1(pd_model, numSamples = 4096, nFFT = 4096):
    pd_model = pd_model.iloc[0:0]
    count = 0
    for i in range(int(width)):
        for j in range(int(height)):
            tossValue = random.uniform(0, 1)
            if tossValue < density_of_sensors:
                xPos = i
                yPos = j
                pd_model = pd_model.append({'xPos': i,
                                            'yPos': j,
                                            'numSamples': numSamples,
                                            'nFFT': nFFT}, ignore_index=True)
                pd_model = pd_model.append({'xPos': i + 1,
                                        'yPos': j,
                                        'numSamples': numSamples,
                                        'nFFT': nFFT}, ignore_index=True)

    #pd_model = pd_model[(pd_model['numSamples'] == 1024) & (pd_model['nFFT'] == 256)]
    return pd_model

#Generate sensor data for case 2
#Case 2 -> Sensors have different parameters, selected randomly
def get_case_2(pd_model):
    pd_model = pd_model.iloc[0:0]
    for i in range(int(width)):
        for j in range(int(height)):
            tossValue = random.uniform(0, 1)
            if tossValue < density_of_sensors:
                numSamplesBits = random.randint(9, 12)
                nFFTBits = random.randint(8, numSamplesBits)
                #print (numSamplesBits, nFFTBits)
                pd_model = pd_model.append({'xPos': i,
                                        'yPos': j,
                                        'numSamples': 2 ** numSamplesBits,
                                        'nFFT': 2 ** nFFTBits}, ignore_index=True)
    return pd_model

#Generate sensor data for case 3
#CAse 3 -> sensors can have many parameters. Parameters selected by algorithm
def get_case_3(pd_model):
    pd_model = pd_model.iloc[0:0]
    for i in range(int(width)):
        for j in range(int(height)):
            tossValue = random.uniform(0, 1)
            if tossValue < density_of_sensors:
                for numSamplesBits in range(9, 13):
                    for nFFTBits in range(10, numSamplesBits):
                        pd_model = pd_model.append({'xPos': i,
                                                    'yPos': j,
                                                    'numSamples': 2 ** numSamplesBits,
                                                    'nFFT': 2 ** nFFTBits},
                                                    ignore_index = True)
    return pd_model

#Relevance of a sensor by looking at mutual information
def compute_relevance(i, simulated_data, intruder_present, intruder_absent, pd_model):
    true_positives = len(simulated_data[(simulated_data['t_loc_x'] <= width)
                                    & (simulated_data['s' + str(i)] == 1)])
    true_negatives = len(simulated_data[(simulated_data['t_loc_x'] > width)
                                    & (simulated_data['s' + str(i)] == 0)])
    false_positives = len(simulated_data[(simulated_data['t_loc_x'] > width)
                                     & (simulated_data['s' + str(i)] == 1)])
    false_negatives = len(simulated_data[(simulated_data['t_loc_x'] <= width)
                                     & (simulated_data['s' + str(i)] == 0)])
    #print('true_positives = ', true_positives)
    true_p = true_positives / intruder_present
    true_n = true_negatives / intruder_absent
    false_p = false_positives / intruder_absent
    false_n = false_negatives / intruder_present
    intruder_present /= simulated_data.shape[0]
    intruder_absent /= simulated_data.shape[0]
    positives = true_p + false_p
    #negatives = true_n + false_n
    mi = 0
    if (true_p > 0 and positives > 0):
        tmi = true_p * np.log10((true_p) / (intruder_present * positives))
        mi += tmi
    # if (true_n > 0 and negatives > 0):
    #     tmi = true_n * np.log10((true_n) / (intruder_absent * negatives))
    #     mi += tmi
    # if (false_p > 0 and positives > 0):
    #     tmi = false_p * np.log10((false_p) / (intruder_absent * positives))
    #     mi += tmi
    # if (false_n > 0 and negatives > 0):
    #     tmi = false_n * np.log10((false_n) / (intruder_present * negatives))
    #     mi += tmi
    #pd_model = pd_model.reset_index()
    #print(pd_model)
    #print ("i = ", i, pd_model.loc[i, 'xPos'], pd_model.loc[i, 'yPos'], mi)
    return mi

#Redundancy of a sensor by looking at other selected sensors
def compute_redundancy(i, simulated_data, selected_set):
    nss = simulated_data.shape[1] - 2
    sum = 0
    if (len(selected_set) == 0):
        return 0

    sensorColumnName = 's' + str(i)
    for sensor2Index in selected_set:
        sensor2ColumnName = 's' + str(sensor2Index)
        #distance = euclidean(pd_model.loc[i, 'xPos'], pd_model.loc[i, 'yPos'],
        #          pd_model.loc[sensor2Index, 'xPos'], pd_model.loc[sensor2Index, 'yPos'])
        #if (distance > 75):
        #    continue

        hx = 0
        hu = 0
        for x in [0, 1]:
            nx = len(simulated_data[simulated_data[sensor2ColumnName] == x])
            px = nx / simulated_data.shape[0]
            hx -= px * np.log10(px)

        for u in [0, 1]:
            nu = len(simulated_data[simulated_data[sensorColumnName] == u])
            pu = nu / simulated_data.shape[0]
            hu -= pu * np.log10(pu)

        norm_term = hx
        if hu < hx:
            norm_term = hu
        #norm_term = 1
        #for x in [0, 1]:
        #    for u in [0,1]:
        x = 1
        u = 1
        pux = len(simulated_data[(simulated_data[sensorColumnName] == u)
                     & (simulated_data[sensor2ColumnName] == x)]) / simulated_data.shape[0]
        pu = len(simulated_data[(simulated_data[sensorColumnName] == u)]) / simulated_data.shape[0]
        px = len(simulated_data[(simulated_data[sensor2ColumnName] == x)]) / simulated_data.shape[0]

        if (pux > 0 and pu > 0 and px > 0):
            sum += (pux * np.log10(pux / (pu * px))) / norm_term

    redundancy = sum / len(selected_set)
    return redundancy

#Compute Minimum Redundancy Maximum Relevance (MRMR) metric
def compute_rel_red_metric(sensorNum, simulated_data, selected_set, pd_model):
    num_intruder_absent = len(simulated_data[simulated_data['t_loc_x'] > width])
    num_intruder_present = simulated_data.shape[0] - num_intruder_absent
    rel = compute_relevance(sensorNum, simulated_data, num_intruder_present, num_intruder_absent, pd_model )
    red = compute_redundancy(sensorNum, simulated_data, selected_set)

    #print ('Redundancy = ', sensorNum, rel, red, rel - red)
    return (rel - red)

#Compute cost of selected set
def cost(selected_set, cost_value):
    sum = 0
    for i in selected_set:
        sum += cost_value[i]
    return sum

#Select sensors for each of the three algorithms
import operator
def select_sensors(pd_model, simulated_data, cost_table=None, budget = 10, greedy=False, case_3 = False):
    nss = pd_model.shape[0]
    intruder_present = len(simulated_data[simulated_data['t_loc_x'] > width])
    intruder_absent = simulated_data.shape[0] - intruder_present
    mi = np.zeros(nss)
    selected_set = []
    leftOverSet = [i for i in range(nss)]

    tot_cost = 0
    while (tot_cost <= budget):
        #leftOverSet = [i for i in range(nss) if i not in selected_set]
        if greedy:
            metric_value = Parallel(n_jobs=mp.cpu_count())((delayed(compute_rel_metric)
                                         (i, simulated_data, selected_set, pd_model) for i in leftOverSet))
        else:
            metric_value = Parallel(n_jobs=mp.cpu_count())((delayed(compute_rel_red_metric)
                                        (i, simulated_data, selected_set, pd_model) for i in leftOverSet))
        metric_value = metric_value[:len(leftOverSet)]
        cost_value = [1 for i in range(len(metric_value))]
        if (cost_table is not None):
            cost_value = np.zeros(len(metric_value))
            for i in range(len(leftOverSet)):
                numSamples = pd_model.loc[leftOverSet[i], 'numSamples']
                nFFT = pd_model.loc[leftOverSet[i], 'nFFT']
                #print(cost_table)
                cost_value[i] = cost_table.loc[(cost_table['numSamples'] == numSamples)
                                                    & (cost_table['nFFT'] == nFFT), 'cost'].values[0]

        metric_value = [metric_value[i] / cost_value[i] for i in range(0, len(metric_value))]
        #print('metric_value = ', metric_value, nss, leftOverSet)
        max_metric_index, _ = max(enumerate(metric_value), key=operator.itemgetter(1))

        selected_set.append(leftOverSet[max_metric_index])
        #print('max_metric_index = ', max_metric_index, selected_set, leftOverSet)
        #print ('leftOverSet = ', leftOverSet)
        tot_cost += cost_value[max_metric_index]

        if (case_3):
            xPos, yPos = pd_model.loc[max_metric_index, 'xPos'], pd_model.loc[max_metric_index, 'yPos']
            remove_candidate = (pd_model.loc[:, 'xPos'] == xPos) & (pd_model.loc[:, 'yPos'] == yPos)
            remove_candidate = remove_candidate[remove_candidate].index
            for item in remove_candidate:
                del leftOverSet[item]
        else:
            del leftOverSet[max_metric_index]
        if (len(metric_value) == 1):
            redundant_sensor = [i for i in range(len(metric_value)) if metric_value[i] == 0]
            for sensor in redundant_sensor:
                del leftOverSet[redundant_sensor]


        print('### Selected ', len(selected_set), 'sensors')

    return selected_set

#Compute relevance of a sensor
def compute_rel_metric(sensorNum, simulated_data, selected_set, pd_model):
    num_intruder_absent = len(simulated_data[simulated_data['t_loc_x'] > width])
    num_intruder_present = simulated_data.shape[0] - num_intruder_absent
    rel = compute_relevance(sensorNum, simulated_data, num_intruder_present, num_intruder_absent, pd_model )
    #print (num_intruder_present, num_intruder_absent)
    return (rel)

#Run one instance of Chair Varshney sensor fusion
def chair_varshney_instance(selected_set, pd_model, simulated_data):
    correctInference = False
    globalDecision = 0

    location = (simulated_data.loc['t_loc_x'], simulated_data.loc['t_loc_y'])
    location = (int(location[0]), int(location[1]))
    cvr = 0
    for i in selected_set:
        xPos, yPos = pd_model.loc[i, 'xPos'], pd_model.loc[i, 'yPos']
        numSamples = pd_model.loc[i, 'numSamples']
        nFFT = pd_model.loc[i, 'nFFT']
        distance = euclidean(location[0], location[1], xPos, yPos)
        pd = get_pd_val(numSamples, nFFT, distance)
        #print (numSamples, nFFT, distance, pd)

        decision = simulated_data.loc['s' + str(i)]
        if (pd > pfa):
            if (decision == 1):
                cvr += np.log10(pd / pfa)
            else:
                cvr += np.log10((1 - pd) / (1 - pfa))
            #print(pd, pfa, cvr)
    if (cvr > 0):
        globalDecision = 1
    if ((simulated_data.loc['t_loc_x'] > width) and (globalDecision == 0)):
        correctInference = True
        #print ("Correctly inferred absence")
    if ((simulated_data.loc['t_loc_x'] <= width) and (globalDecision == 1)):
        correctInference = True
        #print("Correctly inferred absence")
    # if ((simulated_data.loc['t_loc_x'] > width) and (globalDecision == 1)):
    #     print("Wrongly inferred presence")
    # if ((simulated_data.loc['t_loc_x'] <= width) and (globalDecision == 0)):
    #     print("Wrongly inferred absence")

    return correctInference

def chair_varshney_instance_efficient_with_intruder(selected_set, pd_model, simulated_data):
    correct_detect = np.zeros(len(selected_set) + 1, dtype=bool)

    location = (simulated_data.loc['t_loc_x'], simulated_data.loc['t_loc_y'])
    location = (location[0], location[1])
    cvr = np.zeros(len(selected_set) + 1)
    for i in range(1, len(selected_set) + 1):
        sensorNum = selected_set[i - 1]
        xPos, yPos = pd_model.loc[sensorNum, 'xPos'], pd_model.loc[sensorNum, 'yPos']
        numSamples = pd_model.loc[sensorNum, 'numSamples']
        nFFT = pd_model.loc[sensorNum, 'nFFT']
        distance = euclidean(location[0], location[1], xPos, yPos)
        pd = get_pd_val(numSamples, nFFT, distance)
        #print (numSamples, nFFT, distance, pd)
        cvr[i] = cvr[i-1]
        decision = simulated_data.loc['s' + str(sensorNum)]
        if (pd > pfa):
            if (decision == 1):
                cvr[i] += np.log10(pd / pfa)
            else:
                cvr[i] += np.log10((1 - pd) / (1 - pfa))
            #print(pd, pfa, cvr)
        if (cvr[i] > 0):
            globalDecision = 1
        else:
            globalDecision = 0
        #print ("Correctly inferred absence")
        if ((simulated_data.loc['t_loc_x'] <= width) and (globalDecision == 1)):
            correct_detect[i] = True
        #print("Correctly inferred absence")
    #print('cvr = ', cvr)
    return correct_detect

def chair_varshney_instance_efficient_without_intruder(selected_set, pd_model, simulated_data):
    detect = np.zeros(len(selected_set) + 1, dtype=bool)

    location = (simulated_data.loc['t_loc_x'], simulated_data.loc['t_loc_y'])
    location = (location[0], location[1])
    cvr = np.zeros((len(selected_set) + 1, width, width))
    for i in range(1, len(selected_set) + 1):
        sensorNum = selected_set[i - 1]
        xPos, yPos = pd_model.loc[sensorNum, 'xPos'], pd_model.loc[sensorNum, 'yPos']
        numSamples = pd_model.loc[sensorNum, 'numSamples']
        nFFT = pd_model.loc[sensorNum, 'nFFT']
        for loc1 in range(width):
            for loc2 in range(width):
                distance = euclidean(loc1, loc2, xPos, yPos)
                pd = get_pd_val(numSamples, nFFT, distance)
                # print (numSamples, nFFT, distance, pd)

                cvr[i][loc1][loc2] = cvr[i-1][loc1][loc2]
                decision = simulated_data.loc['s' + str(sensorNum)]
                if (pd > pfa):
                    if (decision == 1):
                        cvr[i][loc1][loc2] += np.log10(pd / pfa)
                    else:
                        cvr[i][loc1][loc2] += np.log10((1 - pd) / (1 - pfa))
                        # print(pd, pfa, cvr)
    for i in range(1, len(selected_set) + 1):
        if (np.any(cvr[i][:][:] > 0)):
            detect[i] = 1
    #print(np.sum(detect), cvr)
    return detect

import time
#Wrapper of Chair Varshney fusion rule
def chair_varshney(pd_model, selected_set, simulated_data):
    #pd_map = construct_pd_map(selected_set)

    output1 = Parallel(n_jobs = mp.cpu_count())((delayed(chair_varshney_instance_efficient_with_intruder)
                          (selected_set, pd_model, simulated_data.loc[i, :]) for i in range(len(simulated_data) // 2)))
    output2 = Parallel(n_jobs=mp.cpu_count())((delayed(chair_varshney_instance_efficient_without_intruder)
                                              (selected_set, pd_model, simulated_data.loc[i, :]) for i in
                                              range(len(simulated_data) // 2, len(simulated_data))))
    #output2 = np.zeros((1, 1))
    #print(output)
    detect_performance = np.zeros(int(output1[0].shape[0]))
    false_positive = np.zeros(int(output2[0].shape[0]))
    for simNo in range(len(output1)):
        for sensorCnt in range(1, output1[simNo].shape[0]):
            if (output1[simNo][sensorCnt] == True):
                detect_performance[sensorCnt] += 1
    for simNo in range(len(output2)):
        for sensorCnt in range(1, output2[simNo].shape[0]):
            if (output2[simNo][sensorCnt] == True):
                false_positive[sensorCnt] += 1

    detect_performance_mean = np.divide(detect_performance, len(simulated_data) // 2)
    detect_performance_var = np.zeros(output1[0].shape[0])
    for sensorCnt in range(1, output1[0].shape[0]):
        detect_performance_var += (output1[:][sensorCnt] - detect_performance_mean[sensorCnt]) ** 2
    detect_performance_var /= len(simulated_data) // 2
    detect_performance_var = np.sqrt(detect_performance_var)

    #print(detect_performance, false_positive)
    false_positive_mean = np.divide(false_positive, len(simulated_data) // 2)
    false_positive_var = np.zeros(output2[0].shape[0])
    for sensorCnt in range(1, output1[0].shape[0]):
        false_positive_var += (output2[:][sensorCnt] - false_positive_mean[sensorCnt]) ** 2
    false_positive_var /= len(simulated_data) // 2
    false_positive_var = np.sqrt(false_positive_var)

    return detect_performance_mean, detect_performance_var, false_positive_mean, false_positive_var

#One instance of prior probability
def trial_prior():
    x, y = np.meshgrid(np.linspace(-1, 1, 11), np.linspace(-1, 1, 6))
    #print (x, y)
    d = np.sqrt(x*x + y*y)
    sigma, mu = 0.25, 0
    g1 = np.exp(-( (d-mu)**2 / ( 2.0 * sigma**2 ) ) )
    g2 = g1 * 1
    g = np.concatenate((g1, g2), axis=0)
    # sigma, mu = 1.0, 0.5
    # g2 = np.exp(-( (d-mu)**2 / ( 2.0 * sigma**2 ) ) )
    # g = np.add(g1, g2)

    plt.imshow(g, cmap='hot')
    plt.colorbar()
    plt.show()
    return g

#Second instance of prior probability
def trial_prior2(pd_model):
    numSamples = 32
    nFFT = 32
    g = np.zeros((100, 100))
    #pd_model = pd_model.iloc[0:0]
    for i in range(5, 15):
        for j in range(5, 15):
            g[i, j] = 4
            # pd_model = pd_model.append({'xPos': i,
            #                             'yPos': j,
            #                             'numSamples': numSamples,
            #                             'nFFT': nFFT}, ignore_index=True)
    for i in range(70, 90):
        for j in range(70, 90):
            g[i, j] = 1
            # pd_model = pd_model.append({'xPos': i,
            #                             'yPos': j,
            #                             'numSamples': numSamples,
            #                             'nFFT': nFFT}, ignore_index=True)
    for i in range(5, 20):
        for j in range(70, 90):
            g[i, j] = 2

    return g, pd_model

#Third instance of prior probability
def trial_prior3(pd_model):
    numSamples = 32
    nFFT = 32
    g = np.zeros((100, 100))
    #pd_model = pd_model.iloc[0:0]
    for i in range(5, 6):
        for j in range(5, 6):
            g[i, j] = 1
            # pd_model = pd_model.append({'xPos': i,
            #                             'yPos': j,
            #                             'numSamples': numSamples,
            #                             'nFFT': nFFT}, ignore_index=True)
    for i in range(95, 96):
        for j in range(95, 96):
            g[i, j] = 2

    return g, pd_model

#Sensors for building a simple test case
def trial_sensors(pd_model):
    pd_model = pd_model.iloc[0:0]
    nFFT = 32
    numSamples = 512

    xPos = [15, 15, 15, 19, 85, 85, 82, 85, 87, 90]
    yPos = [3, 7, 6, 7, 8, 95, 97, 97, 95, 94]

    for i in range(0, len(xPos)):
        pd_model = pd_model.append({'xPos': xPos[i],
                                'yPos': yPos[i],
                                'numSamples': numSamples,
                                'nFFT': nFFT}, ignore_index=True)
    return pd_model

def read_energy(filename):
    df = pd.read_csv(filename, header=None)
                     # names=['numSamples',
                     #        'nFFT',
                     #        'snr',
                     #        'cost',
    #print (df)
    df.columns = ['numSamples', 'nFFT', 'pd', 'cost']
    #df = df.drop(['pd'])
    #array = df.loc['cost']
    #max_value = np.max(array)
    df = df.drop('pd', axis=1)
    df.loc[:, 'cost'] =  df.loc[:, 'cost'] / np.max(df.loc[:, 'cost'])

    return df

def compute_distance_heuristic(predictor):
    pd = 1
    distance = 1
    while (pd > pfa):
        pd = get_pd_val(4096, 4096, distance)
        distance += 1
    return distance

def find_avg_distance_sensors(pd_model):
    distance = np.zeros(pd_model.shape[0] * (pd_model.shape[0] - 1))
    k = 0
    for key,row in pd_model.iterrows():
        for key2, row2 in pd_model.iterrows():
            if key != key2:
                distance[k] = euclidean(row['xPos'], row['yPos'], row2['xPos'], row2['yPos'])
                k += 1
    percentile_values = np.percentile(distance, [1, 10, 25, 50, 75, 90])
    #print('Percentile values = ', percentile_values)

#Read prior probability from prior map
def get_priors():
    pre_prior = np.loadtxt('prior.dat')
    priors = np.zeros((pre_prior.shape[0] * 10, pre_prior.shape[1] * 10))
    for i in range(pre_prior.shape[0]):
        for j in range(pre_prior.shape[1]):
            for k in range(0, 10):
                priors[i * 10, j * 10 + k] = pre_prior[i, j]
    return priors

from pathlib import Path
import pickle

#Infocom Case 1
def infocom_select_sensors(pd_model, simulated_data, budget = None, cost_table = None, case_3=False):

    #print(pd_model)
    cluster_sum_prior = np.zeros(len(cluster_list))
    for clusternum in range(len(cluster_list)):
        cluster = cluster_list[clusternum]
        cluster_sum_prior[clusternum] = np.sum(priors[cluster[0]:cluster[1], cluster[2]:cluster[3]])

    num_intruder_absent = len(simulated_data[simulated_data['t_loc_x'] > width])
    num_intruder_present = simulated_data.shape[0] - num_intruder_absent
    true_positive_rate = np.zeros(len(pd_model))
    global_score = np.zeros(len(pd_model))
    for clusternum in range(len(cluster_list)):
        cluster = cluster_list[clusternum]
        list_of_relevant_sensors = (pd_model.loc[:, 'xPos'] >= cluster[0]) & \
                                   (pd_model.loc[:, 'xPos'] <= cluster[1]) & \
                                   (pd_model.loc[:, 'yPos'] >= cluster[2]) & \
                                   (pd_model.loc[:, 'yPos'] <= cluster[3])
        list_of_relevant_sensors = pd_model[list_of_relevant_sensors].index
        for sensornum in list_of_relevant_sensors:
            true_positive_rate[sensornum] = len(simulated_data[(simulated_data['t_loc_x'] <= width)
                                       & (simulated_data['s' + str(sensornum)] == 1)]) / num_intruder_present
            global_score[sensornum] = cluster_sum_prior[clusternum] / true_positive_rate[sensornum]

    if cost_table is None:
        list_of_relevant_sensors = np.argsort(global_score)
    else:
        cost_value = np.zeros(len(global_score))
        for i in range(len(global_score)):
            numSamples = pd_model.loc[i, 'numSamples']
            nFFT = pd_model.loc[i, 'nFFT']
            # print(cost_table)
            cost_value[i] = cost_table.loc[(cost_table['numSamples'] == numSamples)
                                           & (cost_table['nFFT'] == nFFT), 'cost'].values[0]

        list_of_relevant_sensors = np.argsort(np.divide(global_score, cost_value))
        x_dict = []
        y_dict = []
        if case_3 is True:
            pd_model_copy = pd_model.copy()
            pd_model_copy['global_score'] = global_score
            pd_model_copy = pd_model_copy.sort_values('global_score', ascending=False).drop_duplicates(['xPos', 'yPos'])
            list_of_relevant_sensors = pd_model_copy.index.values

    #print(list_of_relevant_sensors)
    if budget is None:
        return list_of_relevant_sensors
    else:
        return (list_of_relevant_sensors[0:budget])


#Run case 1. These results are used in paper plots
def run_case_1(pd_model, max_cost, skip_step=1):
    pd_model = get_case_1(pd_model)
    pd_model = pd_model.reset_index(drop=True)
    # pd_model = pd_model.sample(frac=1).reset_index(drop=True)
    # pd_model = trial_sensors(pd_model)

    # pd_model = pd_model.head()
    #print(pd_model.loc[:, 'xPos'], pd_model.loc[:, 'yPos'])
    simulated_data = simulate(pd_model)
    simulated_data = assign_column_names(simulated_data, pd_model.shape[0])

    # simulated_data = simulated_data.sample(frac=0.1)
    # simulated_data = simulated_data.reset_index()
    #print('simulated_data generated', simulated_data)
    #print(simulated_data.loc[:, 't_loc_x'], simulated_data.loc[:, 't_loc_y'])
    sensor_select_file = 'case_1_sensor_select'
    greedy_select_file = 'case_1_greedy_select'
    with open(sensor_select_file, 'a+') as f:
        sensor_subset = []
        f.seek(0)
        for line in f:
            sensor_subset.append(int(line.strip()))
        if (len(sensor_subset) < max_cost):
            f.seek(0)
            sensor_subset = select_sensors(pd_model, simulated_data, budget=max_cost)
            for sensor in sensor_subset:
                f.write(str(sensor) + '\n')

    with open(greedy_select_file, 'a+') as f:
        f.seek(0)
        greedy_sensor_subset = []
        for line in f:
            greedy_sensor_subset.append(int(line.strip()))
        if (len(greedy_sensor_subset) < max_cost):
            f.seek(0)
            greedy_sensor_subset = select_sensors(pd_model, simulated_data, budget=max_cost, greedy=True)
            for sensor in greedy_sensor_subset:
                f.write(str(sensor) + '\n')
    infocom_subset = infocom_select_sensors(pd_model, simulated_data, budget=max_cost)
    # sensor_subset_loc = [(sensor_num, pd_model.loc[sensor_num, 'xPos'], pd_model.loc[sensor_num, 'yPos']) for
    #                      sensor_num in sensor_subset]
    # greedy_sensor_subset_loc = [(sensor_num, pd_model.loc[sensor_num, 'xPos'], pd_model.loc[sensor_num, 'yPos']) for
    #                             sensor_num in greedy_sensor_subset]
    #print(sensor_subset_loc, greedy_sensor_subset_loc)

    detection_accuracy, detection_std, false_positive, false_positive_std = chair_varshney(pd_model, sensor_subset, simulated_data)
    greedy_detection_accuracy, greedy_detection_std, greedy_false_positive, greedy_false_std = chair_varshney(pd_model, greedy_sensor_subset, simulated_data)
    infocom_accuracy, infocom_accuracy_std, infocom_false, infocom_false_std = chair_varshney(pd_model, infocom_subset, simulated_data)

    #print(len(detection_accuracy), detection_accuracy[0], detection_accuracy[1])
    random_detection_accuracy = np.zeros(max_cost + 1)
    random_false_positive = np.zeros(max_cost + 1)

    # for i in range(0, 10):
    #      random_sensor_subset = random.sample(range(0, pd_model.shape[0]), max_cost)
    #      random_detection_accuracy_instance, random_false_positive_instance = chair_varshney(pd_model, random_sensor_subset, simulated_data)
    #      #print(random_detection_accuracy_instance)
    #      random_detection_accuracy = np.add(random_detection_accuracy, random_detection_accuracy_instance)
    #      random_false_positive = np.add(random_false_positive, random_false_positive_instance)
    # random_detection_accuracy = np.divide(random_detection_accuracy, 10)

    for budget in range(0, max_cost + 1, skip_step):
        print(' >>> ', budget, detection_accuracy[budget], detection_std[budget],
              greedy_detection_accuracy[budget], greedy_detection_std[budget],
              infocom_accuracy[budget], infocom_accuracy_std[budget],
              random_detection_accuracy[budget])
    for budget in range(0, max_cost + 1, skip_step):
        print(' >>> ', budget, false_positive[budget], false_positive_std[budget],
              greedy_false_positive[budget], greedy_false_std[budget],
              infocom_false[budget], infocom_false_std[budget],
              random_false_positive[budget])


#Run case 2. These results are used to plot experiments.
def run_case_2(max_cost, cost_table, pd_model):
    pd_model = get_case_2(pd_model)
    pd_model = pd_model.reset_index(drop=True)
    # pd_model = pd_model.sample(frac=1).reset_index(drop=True)
    #pd_model = trial_sensors(pd_model)

    # pd_model = pd_model.head()
    print('pd_model generated')
    simulated_data = simulate(pd_model)
    simulated_data = assign_column_names(simulated_data, pd_model.shape[0])

    #simulated_data = simulated_data.sample(frac=0.1)
    simulated_data = simulated_data.reset_index()
    #print('simulated_data generated', simulated_data)

    sensor_select_file = 'case_2_sensor_select' + str(max_cost)
    greedy_select_file = 'case_2_greedy_select' + str(max_cost)
    with open(sensor_select_file, 'a+') as f:
        sensor_subset = []
        f.seek(0)
        for line in f:
            sensor_subset.append(int(line.strip()))
        if (len(sensor_subset) == 0):
            f.seek(0)
            print('Hello')
            sensor_subset = select_sensors(pd_model, simulated_data, cost_table, budget=max_cost)
            for sensor in sensor_subset:
                f.write(str(sensor) + '\n')

    with open(greedy_select_file, 'a+') as f:
        f.seek(0)
        greedy_sensor_subset = []
        for line in f:
            greedy_sensor_subset.append(int(line.strip()))
        if (len(greedy_sensor_subset) == 0):
            f.seek(0)
            print('Hello')
            greedy_sensor_subset = select_sensors(pd_model, simulated_data, cost_table, budget=max_cost, greedy=True)
            for sensor in greedy_sensor_subset:
                f.write(str(sensor) + '\n')

    infocom_subset = infocom_select_sensors(pd_model, simulated_data, budget=None, cost_table=cost_table)

    detection_accuracy, detection_std, detection_false, false_std = chair_varshney(pd_model, sensor_subset[0:50], simulated_data)
    greedy_detection_accuracy, greedy_std, greedy_false, greedy_false_std = chair_varshney(pd_model, greedy_sensor_subset[0:50], simulated_data)
    infocom_accuracy, infocom_std, infocom_false, infocom_false_std = chair_varshney(pd_model, infocom_subset[0:50], simulated_data)

    print('Our Technique')
    tot_cost = 0
    print(sensor_subset, detection_accuracy)
    for i in range(1, len(sensor_subset)):
        nFFT = pd_model.loc[sensor_subset[i], 'nFFT']
        numSamples = pd_model.loc[sensor_subset[i], 'numSamples']
        cost = cost_table.loc[(cost_table['numSamples'] == numSamples) & (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
        tot_cost += cost
        print (tot_cost, detection_accuracy[i], detection_std[i], detection_false[i], false_std[i])

    print('Greedy Technique')
    tot_cost = 0
    for i in range(1, len(greedy_sensor_subset)):
        nFFT = pd_model.loc[greedy_sensor_subset[i], 'nFFT']
        numSamples = pd_model.loc[greedy_sensor_subset[i], 'numSamples']
        cost = cost_table.loc[(cost_table['numSamples'] == numSamples) & (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
        tot_cost += cost
        print(tot_cost, greedy_detection_accuracy[i], greedy_std[i], greedy_false[i], greedy_false_std[i])

    print('Infocom Technique')
    tot_cost = 0
    for i in range(1, len(greedy_sensor_subset)):
        nFFT = pd_model.loc[greedy_sensor_subset[i], 'nFFT']
        numSamples = pd_model.loc[greedy_sensor_subset[i], 'numSamples']
        cost = cost_table.loc[(cost_table['numSamples'] == numSamples) & (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
        tot_cost += cost
        print(tot_cost, infocom_accuracy[i], infocom_std[i], infocom_false[i], infocom_false_std[i])

    # print("Random Cost")
    # avg_accuracy = np.zeros(100)
    # for cnt in range(0, 10):
    #     rand_sequence = np.random.permutation(len(pd_model))
    #     i = 0
    #     cost_list = []
    #     tot_cost = 0
    #     while (tot_cost < max_cost):
    #         index = rand_sequence[i]
    #         nFFT = pd_model.loc[index, 'nFFT']
    #         numSamples = pd_model.loc[index, 'numSamples']
    #         cost = cost_table.loc[(cost_table['numSamples'] == numSamples) &
    #                               (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
    #         tot_cost += cost
    #         cost_list.append(tot_cost)
    #         i += 1
    #
    #         random_detection_accuracy, random_false = chair_varshney(pd_model, rand_sequence[:index], simulated_data)
    #         #avg_accuracy[i] /= 10
    #         print(cost_list[i-1], random_detection_accuracy[i], random_false[i])

#Run case 3. These results are used to plot experiments.
def run_case_3(max_cost, cost_table, pd_model):
    pd_model = get_case_3(pd_model)
    pd_model = pd_model.reset_index(drop=True)
    # pd_model = pd_model.sample(frac=1).reset_index(drop=True)
    # pd_model = trial_sensors(pd_model)

    # pd_model = pd_model.head()
    simulated_data = simulate(pd_model)
    simulated_data = assign_column_names(simulated_data, pd_model.shape[0])
    # simulated_data = simulated_data.sample(frac=0.1)
    # simulated_data = simulated_data.reset_index()
    print('simulated_data generated', simulated_data.shape)

    sensor_select_file = 'case_3_sensor_select' + str(max_cost)
    greedy_select_file = 'case_3_greedy_select' + str(max_cost)
    with open(sensor_select_file, 'a+') as f:
        sensor_subset = []
        f.seek(0)
        for line in f:
            sensor_subset.append(int(line.strip()))
        if (len(sensor_subset) == 0):
            f.seek(0)
            #print('Hello')
            sensor_subset = select_sensors(pd_model, simulated_data, cost_table, budget=max_cost)
            for sensor in sensor_subset:
                f.write(str(sensor) + '\n')

    with open(greedy_select_file, 'a+') as f:
        f.seek(0)
        greedy_sensor_subset = []
        for line in f:
            greedy_sensor_subset.append(int(line.strip()))
        if (len(greedy_sensor_subset) == 0):
            f.seek(0)
            #print('Hello')
            greedy_sensor_subset = select_sensors(pd_model, simulated_data, cost_table, budget=max_cost, greedy=True)
            for sensor in greedy_sensor_subset:
                f.write(str(sensor) + '\n')

    infocom_subset = infocom_select_sensors(pd_model, simulated_data, budget=None, cost_table=cost_table, case_3 = True)

    detection_accuracy, detection_std, detection_false, detection_false_std = chair_varshney(pd_model, sensor_subset, simulated_data)
    greedy_detection_accuracy, greedy_std, greedy_false, greedy_false_std = chair_varshney(pd_model, greedy_sensor_subset, simulated_data)
    infocom_accuracy, infocom_std, infocom_false, infocom_false_std = chair_varshney(pd_model, infocom_subset[0:50], simulated_data)
    random_sensor_cost = cost_table.loc[(cost_table['numSamples'] == 256) & (cost_table['nFFT'] == 256), 'cost']
    #
    tot_cost = 0
    for i in range(1, len(sensor_subset)):
        nFFT = pd_model.loc[sensor_subset[i], 'nFFT']
        numSamples = pd_model.loc[sensor_subset[i], 'numSamples']
        cost = cost_table.loc[(cost_table['numSamples'] == numSamples) & (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
        tot_cost += cost
        print (tot_cost, detection_accuracy[i], detection_std[i],
               detection_false[i], detection_false_std[i])

    print('Greedy Technique')
    tot_cost = 0
    for i in range(1, len(greedy_sensor_subset)):
        nFFT = pd_model.loc[greedy_sensor_subset[i], 'nFFT']
        numSamples = pd_model.loc[greedy_sensor_subset[i], 'numSamples']
        cost = cost_table.loc[(cost_table['numSamples'] == numSamples) &
                              (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
        tot_cost += cost
        print(tot_cost, greedy_detection_accuracy[i], greedy_std[i], greedy_false[i], greedy_false_std[i])

    print('Infocom Technique')
    tot_cost = 0
    for i in range(1, len(infocom_subset)):
        nFFT = pd_model.loc[infocom_subset[i], 'nFFT']
        numSamples = pd_model.loc[infocom_subset[i], 'numSamples']
        cost = cost_table.loc[(cost_table['numSamples'] == numSamples) &
                              (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
        tot_cost += cost
        if tot_cost > max_cost:
            break
        print(tot_cost, infocom_accuracy[i], infocom_std[i], infocom_false[i], infocom_false_std[i])


    #random_sensor_subset = random.sample(range(0, pd_model.shape[0]), int(max_cost * random_sensor_cost))
    # subsubset = []
    # for randCnt in range(10):
    #     print("Random Cost")
    #     tot_cost = 0
    #     rand_sequence = np.random.permutation(len(pd_model))
    #     i = 0
    #     cost_list = []
    #     random_sensor_subset = []
    #     while (tot_cost < max_cost):
    #         index = rand_sequence[i]
    #         nFFT = pd_model.loc[index, 'nFFT']
    #         numSamples = pd_model.loc[index, 'numSamples']
    #         cost = cost_table.loc[(cost_table['numSamples'] == numSamples) &
    #                               (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
    #         tot_cost = cost + tot_cost
    #         random_sensor_subset.append(index)
    #         cost_list.append(tot_cost)
    #     random_detection_accuracy, _ = chair_varshney(pd_model, random_sensor_subset, simulated_data)
    #     for i in range(len(random_sensor_subset)):
    #          print(cost_list[i], random_detection_accuracy[i])
#
    #
    #     # sensor_subset_loc = [(sensor_num, pd_model.loc[sensor_num, 'xPos'], pd_model.loc[sensor_num, 'yPos']) for
    #     #                      sensor_num in subsubset]
    #     # greedy_sensor_subset_loc = [(sensor_num, pd_model.loc[sensor_num, 'xPos'], pd_model.loc[sensor_num, 'yPos']) for
    #     #                             sensor_num in
    #     #                            greedy_subsubset]
    #     # print(sensor_subset_loc, greedy_sensor_subset_loc)
    # print('Our Technique')
    # tot_cost = 0
    # for i in range(1, len(sensor_subset)):
    #     nFFT = pd_model.loc[sensor_subset[i], 'nFFT']
    #     numSamples = pd_model.loc[sensor_subset[i], 'numSamples']
    #     cost = cost_table.loc[(cost_table['numSamples'] == numSamples) &
    #                           (cost_table['nFFT'] == nFFT), 'cost'].iloc[0]
    #     tot_cost += cost
    #     print(tot_cost, detection_accuracy[i])



import scipy.special as sp
def get_count_baseline(num_sensors):
    actual_pfa = 1
    j = 0
    for j in range(0, num_sensors): #j is the number of sensors that must say 1
        value = sp.comb(num_sensors, j) * ((1 - pfa) ** (num_sensors - j)) * ((pfa) ** (j))
        actual_pfa -= value
        #print("c", value, actual_pfa)
    if (actual_pfa <= 0.1):
        #print (actual_pfa, j)
        return j
    return num_sensors

def evaluate_parameters_value(pd_model):
    param_list = [(32, 32), (512, 512), (4096, 4096)]

    for params in param_list:
        pd_model = get_case_1(pd_model, params[0], params[1])
        pd_model = pd_model.reset_index(drop=True)
        #print (pd_model)
        #pd_model = pd_model.sample(frac=1).reset_index(drop=True)
        # pd_model = trial_sensors(pd_model)

        # pd_model = pd_model.head()
        # print(pd_model.loc[:, 'xPos'], pd_model.loc[:, 'yPos'])
        simulated_data = simulate(pd_model)
        # simulated_data = simulated_data.sample(frac=0.1)
        # simulated_data = simulated_data.reset_index()
        # print('simulated_data generated', simulated_data)
        # print(simulated_data.loc[:, 't_loc_x'], simulated_data.loc[:, 't_loc_y'])


        sensor_subset = select_sensors(pd_model, simulated_data, budget=10)
        detection_accuracy, _ = chair_varshney(pd_model, sensor_subset, simulated_data)
        print(params, detection_accuracy)

#Another experiment, to evaluate how good Chair Varshney fusion rule is.
def evaluate_chair_varshney(pd_model, num_sensors):
    pd_model = get_case_1(pd_model)
    pd_model = pd_model.reset_index(drop=True)

    simulated_data = simulate(pd_model)
    print('simulation data generated')
    sensor_subset = select_sensors(pd_model, simulated_data, cost_table = None, budget=num_sensors)

    print('sensors selected')
    true_detects, true_std, false_positives, false_std = chair_varshney(pd_model, sensor_subset, simulated_data)
    print(true_detects, false_positives)
    #detection_accuracy = np.add(detection_accuracy_1, detection_accuracy_2)
    print('chair varshney run')

    #baseline technique
    for num_sensors in range(1, num_sensors + 1):
        maxDetection_k = 0
        maxDetection = 0
        baseline_detection = 0
        baseline_false = 0
        k = num_sensors / 4 + 1
        for index, row in simulated_data.iterrows():
            count = 0
            subsubset = sensor_subset[:num_sensors]
            for sensor in subsubset: #Count number of 1's for each simulation
                intruder_present = [False for i in range(len(simulated_data))]
                if (simulated_data.loc[index, 's' + str(sensor)] == 1):
                    count += 1
            if (count >= k and row['t_loc_x'] <= width):
                intruder_present[index] = True
                baseline_detection += 1
                #print (count)
            if (count >= k and row['t_loc_x'] > width):
                intruder_present[index] = False
                baseline_false += 1
        if (baseline_detection > maxDetection):
            maxDetection = baseline_detection
            maxDetection_k = k

        baseline_detection /= len(simulated_data)
        baseline_false /= len(simulated_data)

        total_correct = (true_detects[num_sensors] - false_positives[num_sensors] + 1) / 2
        baseline_correct = (baseline_detection - baseline_false + 1) / 2
        print (num_sensors, true_detects[num_sensors], false_positives[num_sensors], total_correct,
               baseline_detection, baseline_false, baseline_correct)


pd.set_option('display.max_columns', 500)

priors = get_priors()
cum = np.cumsum(priors)

width = len(priors[0])
height = len(priors[1])
cum = np.divide(cum, cum[-1])

import argparse
parser = argparse.ArgumentParser(prog='define_pd.py', description='Select sensors and measure their performance')
parser.add_argument('-c', '--case', default='1', type=int)
parser.add_argument('-t', '--type', default='binary', choices=['binary', 'gaussian'])
parser.add_argument('-s', '--sensors', default='20', type=float)
parser.add_argument('-n', '--simulations', default='2000', type=int)
parser.add_argument('-p', '--pfa', default='0.01', type=float)
parser.add_argument('-d', '--density_sensors', default='0.01', type=float)

args = parser.parse_args()

pfa = args.pfa
density_of_sensors = args.density_sensors

num_simulations = args.simulations
pd.set_option('display.max_rows', None)
random.seed(3)

edf = read_csv('expected_snrmap.dat')
tx_loc = get_transmitter_loc_data(edf)
pd_model = create_pd_model(edf, tx_loc)
pd_model, predictor = sanitize_model(pd_model)

pd_model.columns = ['xPos', 'yPos', 'numSamples', 'nFFT', 'SNR', 'distance', 'pd_emp', 'pd_ana']
pd_model = pd_model.drop(['pd_ana', 'SNR', 'distance'], axis=1)

cluster_list = [(5, 15, 5, 15), (70, 90, 70, 90), (5, 20, 70, 90)]
priors, _ = trial_prior2(pd_model)
cum = np.cumsum(priors)
cum = np.divide(cum, cum[-1])

#np.set_printoptions(threshold=np.nan)
width = len(priors)
height = len(priors)
#for i in range(0, len(cum)):
#    print(i, cum[i])
print(width, height)

cost_table = read_energy('energy.txt')
#print (cost_table)

#print ("Simulated Data = ", simulated_data) #Verified this is working
#find_avg_distance_sensors(pd_model)
# print (pd_model.shape)
import glob, os
for f in glob.glob("case_1_*"):
    os.remove(f)
for f in glob.glob("case_2_*"):
    os.remove(f)

for f in glob.glob("case_3_*"):
    os.remove(f)

#print(pd_model)

if (args.type == 'binary'):
    if (args.case == 1):
        run_case_1(pd_model, int(args.sensors))
    elif (args.case == 2):
        run_case_2(10, cost_table, pd_model)
    elif (args.case == 3):
        run_case_3(10, cost_table, pd_model)
    elif (args.case == 4):
        evaluate_chair_varshney(pd_model, int(args.sensors))
# else:
#     run_case_1(pd_model, 30)
#run_case_1(pd_model, 30)
#run_case_2(0.8, cost_table, pd_model)
#ost_table = cost_table.drop(cost_table[cost_table['numSamples'] <= 512].index)
#run_case_3(10, cost_table, pd_model)
#evaluate_chair_varshney(pd_model)
#print (pd_model, simulated_data)
#evaluate_parameters_value(pd_model)
#print.set
#print (pd_model.to_string())
