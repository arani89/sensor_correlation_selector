import numpy as np
from random import randint
import random
import math
import pandas as pd
import os
import os.path
import shutil
import multiprocessing
import joblib
from joblib import Parallel, delayed
#import matplotlib.pyplot as plt
from scipy.misc import imsave
import scipy

def simulate_one_instance(sf):
    sf._simulate_one_instance()


def _get_transmitter_location():
    tx_x = randint(0, width - 1)
    tx_y = randint(0, length - 1)
    transmitter_location = (tx_x, tx_y)
    return transmitter_location

def _get_sensor_locations():
    sensor_locations = []
    for i in range(0, nss):
        flag = 0
        while (flag == 0):
            tx_x = randint(0, width - 1)
            tx_y = randint(0, length - 1)
            sensor_location = (tx_x, tx_y)
            if (sensor_location not in sensor_locations):
                sensor_locations.append(sensor_location)
                flag = 1
    return sensor_locations

def _euclidean_distance(x1, y1, x2, y2):
    return np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

def _get_received_power(tx_power, tx_loc, my_loc):
    if tx_power != -1:
        tx_power = 10 * np.log10(tx_power)  # milli watts to dBm
        distance = _euclidean_distance(tx_loc[0], tx_loc[1], my_loc[0], my_loc[1])
    else:
        tx_power = 0
        distance = np.sqrt(length ** 2 + width ** 2)

    shadowing_noise = random.gauss(0, sigma)
    sensor_noise = random.gauss(0, sensor_sigma)
    if (distance == 0):
        distance = 1 / 2.0  # center of the same cell #approximation
    rec_power = tx_power - 10 * alpha * np.log10(distance) + shadowing_noise + sensor_noise
    if rec_power < -90:
        rec_power = -90 + shadowing_noise + sensor_noise
    return rec_power

def _get_pd(snr, pfa):
     pd = 0.5 * scipy.special.erfc(scipy.special.erfcinv(2 * pfa) - np.sqrt(snr))
#     pd = 0.5 * special.erfc(special.erfcinv(2 * pfa) - numpy.sqrt(snr))
     return pd

# def _get_pd(tx_loc, sensor_loc):
#     #pd = 0.5 * special.erfc(special.erfcinv(2 * pfa) - numpy.sqrt(snr))
#     #return pd
#     pdVal = 0.9
#     dx = math.fabs(tx_loc[0] - sensor_loc[0]);
#     dy = math.fabs(tx_loc[1] - sensor_loc[1]);
#
#     minVal = min(dx, dy);
#     maxVal = max(dx, dy);
#
#     diagonalSteps = minVal;
#     straightSteps = maxVal - minVal;
#     pdVal -= diagonalSteps * 0.03 + straightSteps * 0.02
#
#     if pdVal < 0:
#         pdVal = 0
#
#     return pdVal

def _get_sensor_output_instance(pd_val):
    val = random.uniform(0, 1)
    output = 0
    if val < pd_val:
        output = 1
    #print (val, pd_val)
    return output

def _get_sensor_output_instance_no_trans(pfa):
    val = random.uniform(0, 1)
    output = 0
    if val < pfa:
        output = 1
    return output

def _get_blank_power(sensor_loc):
    noise_floor = -90
    shadowing_noise = random.gauss(0, sigma)
    sensor_noise = random.gauss(0, sensor_sigma)
    return noise_floor + shadowing_noise + sensor_noise

def _interpolate_output_list(sensor_locs, output_list):
    for i in range(length):
        for j in range(width):
            #print (output_list[i][j])
            output_list[i][j] = 120 + output_list[i][j]
    new_output_list = np.copy(output_list)

    for i in range(length):
        for j in range(width):
            print (output_list[i][j])
        print('\n')
            #output_list[i][j] = 120 + output_list[i][j]

    for i in range(length):
        for j in range(width):
            #print(output_list[sensor_locs[sensorNum][0]][sensor_locs[sensorNum][1]])
            #print (output_list[i][j])
            if (new_output_list[i][j] < 1 and new_output_list[i][j] > -1):
                weight = 0.0
                for sensorNum in range(len(sensor_locs)):
                    distance = np.sqrt((i - sensor_locs[sensorNum][0]) ** 2 + (j - sensor_locs[sensorNum][1]) ** 2)
                    #g_d += 1/distance
                    #print (output_list[sensor_locs[sensorNum][0]][sensor_locs[sensorNum][1]], distance)
                    if (distance > 0):
                        weight += output_list[sensor_locs[sensorNum][0]][sensor_locs[sensorNum][1]] / distance ** 2
                    print (weight)
                new_output_list[i][j] = weight
    # for i in range(length):
    #     for j in range(width):
    #         new_output_list[i][j] -= 120
    print (new_output_list)
    return new_output_list


def simulate_one_instance(sensor_locs):
    tx_loc = _get_transmitter_location()
    label = tx_loc
    output_list = np.zeros((width, length))
    output_list.fill(-100)
    available_values = []
    from scipy.interpolate import Rbf
    x = np.zeros(nss)
    y = np.zeros(nss)
    z = np.zeros(nss)

    for j in range(nss):
        output = _get_received_power(tx_power, tx_loc, sensor_locs[j])
        if output < -90:
            output = -90
        available_values.append(output)
        output_list[sensor_locs[j][0]][sensor_locs[j][1]] = output
        x[j] = sensor_locs[j][0]
        y[j] = sensor_locs[j][1]
        z[j] = output
    tx = np.linspace(0, length, length)
    ty = np.linspace(0, width, width)
    xi, yi = np.meshgrid(tx, ty)
    #rbf = Rbf(x, y, z)
    #output_list = rbf(xi, yi)
    #np.set_printoptions(threshold=np.infty)
    data_frame = pd.DataFrame(output_list)
    #print (data_frame)
    save_data(label, output_list)

    label = (-1, -1)
    for j in range(nss):
        output = _get_blank_power(sensor_locs[j])
        if output < -90:
            output = -90
        output_list[sensor_locs[j][0]][sensor_locs[j][1]] = output
        z[j] = output
    #output_list = _interpolate_output_list(sensor_locs, output_list)
    #rbf = Rbf(x, y, z, epsilon=2)
    #output_list = rbf(xi, yi)
    save_data(label, output_list)

def _run_simulation(sensor_locs, num_simulations):
    Parallel(n_jobs=multiprocessing.cpu_count() - 1)(delayed(simulate_one_instance)(sensor_locs) for i in range(num_simulations))
    #for i in range(num_simulations):
    #   simulate_one_instance(sensor_locs)


def save_data(labels, output_list):
    im_list = []
    validationDir = '/tmp/train'
    trainDir = '/tmp/train'
    # try:
    #     shutil.rmtree(validationDir)
    #     shutil.rmtree(trainDir)
    # except:
    #     pass
    # #os.rmdir('train')
    os.makedirs(validationDir, exist_ok=True)
    os.makedirs(trainDir, exist_ok=True)
    os.makedirs(validationDir + '/0', exist_ok=True)
    os.makedirs(validationDir + '/1', exist_ok=True)
    os.makedirs(trainDir + '/0', exist_ok=True)
    os.makedirs(trainDir + '/1', exist_ok=True)

    output_list[0][0] = 20
    filename = str(int(labels[0])) + '_' + str(int(labels[1]))
    if (labels[0] == -1):
        subdirname = '/0'
    else:
        subdirname = '/1'
    prob = random.uniform(0, 1)
    if (prob < 0.3):
        filePath = validationDir + subdirname + '/' + filename +'_'
    else:
        filePath = trainDir + subdirname + '/' + filename + '_'
    count = 1
    while True:
        #filePath += '_' + str(count)
        if not os.path.exists(filePath + str(count) + '.png'):
            break
        count += 1
    filePath += str(count) + '.png'
    print (filePath)
    #import scipy.ndimage
    #output_list = scipy.ndimage.morphology.grey_closing(output_list, size=(50, 50))
    #plt.imshow(output_list, interpolation='none', origin='lower',vmin=-120, vmax=20)
    #plt.colorbar()
    #plt.savefig(filePath)
    #plt.show()
    #imsave(filePath, output_list)
    # import scipy.ndimage
    #output_list = scipy.ndimage.morphology.grey_closing(output_list, size=(50,50))
    #print (filePath, output_list)
    scipy.misc.toimage(output_list, cmin = -100, cmax=255).save(filePath)


width = 100
length = 100
tx_power = 20
sigma = 15
sensor_sigma = 0
alpha = 10
nss = 1000
pfa = 0.1
num_simulations = 10000
sensor_names = ['s' + str(i) for i in range(0, nss)]
sensor_names.insert(0, 'ty_loc')
sensor_names.insert(0, 'tx_loc')
labels = np.zeros((num_simulations*2, 2))

sensor_locs = _get_sensor_locations()
_run_simulation(sensor_locs, num_simulations)
#sf._fit_data()

#np.set_printoptions(threshold=np.infty)
#sf._save_data()

